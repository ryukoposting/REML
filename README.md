**[Get the daily builds here!](http://dailyprog.org/~ryukoposting/projects/REML/?C=M;O=D)**
## REML: Ryuko's Easy Markup Language
REML is a minimal markup language meant for configuration files that
don't require the sophisticated features of markup languages like
XML, YAML, and TOML. REML is meant to be simple, yet sufficient for
the configuration files needed for small to medium applications.

REML is designed to be extremely easy to parse, making it a strong
option for platforms where resources are limited, speed is essential,
or where reads and writes are made on a regular basis.

REML has 5 types: strings, integers, floats (which are actually doubles),
booleans, and objects. A sample REML file is shown below:
```
# here is a REML comment.
int_example     3
float_example   3.141
str_example     "hello world!"

# objects end with a colon...
obj_example:
    # ...and their members begin with a colon.
    :obj_member_int -3945
    :member_string  "hello again, world!"

# the boolean below doesn't have a colon, so it
# isn't a part of the object above.
    bool_example    true

another_object:
    :member_boolean false
    :some_string    "this is another string"
```
REML objects cannot contain other REML objects; the nature of REML syntax
simply makes nested objects impossible to implement without additional rules
in the syntax, which would contradict the primary objective of REML: simplicity.
