#ifndef REML_H_
#define REML_H_


#ifdef __unix__
#include <stdio.h>
#endif


#ifdef __cplusplus
extern "C" {
#endif

    struct REML_File {
        #ifdef __unix__
        FILE *const file;
        #endif
    };

    enum REML_Type {
        REML_INT,
        REML_FLOAT,
        REML_STRING,
        REML_BOOLEAN,
        REML_OBJECT
    };

    int REML_Init(const struct REML_File *const REML_File);

    int REML_GetInteger(const struct REML_File *const REML_File,
                        char *const element_name);

    double REML_GetFloat(const struct REML_File *const REML_File,
                         char *const element_name);

    char *const REML_GetString(const struct REML_File *const REML_File,
                               char *const element_name);

    unsigned char REML_GetBoolean(const struct REML_File *const REML_File,
                                  char *const element_name);


#ifdef __cplusplus
}
#endif

#endif          // REML_H_
