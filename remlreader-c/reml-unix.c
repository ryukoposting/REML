#include "reml.h"

int REML_CharIsWhiteSpace_(char c) {
    char whitespaces[6] = " \t\n\r\v\f";
    for(int i = 0; i < 6; i++) {
        // printf("char detected: %x %x\n", c, whitespaces[i]);
        if(c == whitespaces[i]) return 1;
    }
    return 0;
}

size_t REML_TrimWhitespace_(char *out, size_t len, const char *const in) {
    const char *str = in;

    // trim leading spaces
    while(REML_CharIsWhiteSpace_(*str)) str++;

    if(*str == 0) {
        *out = 0;
        return 1;
    }

    // trim trailing spaces
    size_t out_size;
    const char *end;

    end = str + strlen(str) - 1;
    while((end > str) && REML_CharIsWhiteSpace_(*end)) end--;
    end++;
    out_size = (end - str) < (len - 1) ? (end - str) : (len - 1);

    memcpy(out, str, out_size);
    out[out_size] = 0;

    return out_size;
}

int REML_Init(const struct REML_File *const REML_File) {

}

int REML_GetInteger(const struct REML_File *const REML_File, char *const element_name) {

}

double REML_GetFloat(const struct REML_File *const REML_File, char *const element_name) {

}

char *const REML_GetString(const struct REML_File *const REML_File, char *const element_name) {

}

unsigned char REML_GetBoolean(const struct REML_File *const REML_File, char *const element_name) {

}
