cmake_minimum_required(VERSION 3.0.0)
project(reml)
set(CMAKE_C_STANDARD 11)

# univ version also works in MinGW and Cygwin, for the Windows folks
# if you want to make a version that uses the Windows API, please do
if (UNIX)
    set(LIB_REML_SOURCES
        reml-unix.c
    )
endif (UNIX)

if (WIN32)
    set(LIB_REML_SOURCES
        reml-win.c
    )
endif (WIN32)

add_library(reml ${LIB_REML_SOURCES})

target_include_directories(reml
    PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}
)
