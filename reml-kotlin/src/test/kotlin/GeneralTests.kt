import io.ryukoposting.reml.Reml
import org.junit.*

class GeneralTests {

    @Test
    fun readFile1() {
        val reml = Reml.loadFromPath("./src/test/resources/testfile_1.reml")

        // check size of REML object
        assert(reml.getSize() == 10)

        // check each individual type
        assert(reml["key_0"] is String)
        assert(reml["key_1"] is Int)
        assert(reml["key_2"] is Double)
        assert(reml["key_3"] is Boolean)
        assert(reml["key_4"] is Boolean)
        assert(reml["key_5"] is Int)
        assert(reml["key_6"] is Double)
        assert(reml["key_7"] is String)
        assert(reml.obj("obj_key")["objkey_0"] is Boolean)
        assert(reml.obj("obj_key")["objkey_1"] is String)
        assert(reml.obj("obj_key2")["objkey2_0"] is Double)
        assert(reml.obj("obj_key2")["objkey2_1"] is String)

        // check each individual value
        assert(reml["key_0"] == "string value for key 0")
        assert(reml["key_1"] == 12345)
        assert(reml["key_2"] == 3.141)
        assert(reml["key_3"] == true)
        assert(reml["key_4"] == false)
        assert(reml["key_5"] == -2)
        assert(reml["key_6"] == -7.12345)
        assert(reml["key_7"] == "not in the object")
        assert(reml.obj("obj_key")["objkey_0"] == false)
        assert(reml.obj("obj_key")["objkey_1"] == "more string")
        assert(reml.obj("obj_key2")["objkey2_0"] == 9.8765)
        assert(reml.obj("obj_key2")["objkey2_1"] == "string with: a colon")

        assert(reml.getSize() == 10)
    }

    @Test
    fun writeFile1() {
        val reml = Reml.loadFromPath("./src/test/resources/testfile_1.reml")
        reml["newkey"] = 6
        assert(reml["newkey"] is Int)
        assert(reml["newkey"] == 6)

        assert(reml.getSize() == 11)

        reml["newkey"] = 6.125
        assert(reml["newkey"] is Double)
        assert(reml["newkey"] == 6.125)

        assert(reml.getSize() == 11)

        reml["newkey"] = "teststring!!!!"
        assert(reml["newkey"] is String)
        assert(reml["newkey"] == "teststring!!!!")

        assert(reml.getSize() == 11)

        reml["fookey"] = true
        assert(reml["fookey"] is Boolean)
        assert(reml["fookey"] == true)

        assert(reml.getSize() == 12)

        reml["newobject", "somekey"] = 10
        reml["newobject", "someotherkey"] = "hello world!"
        assert(reml["newobject"] is Reml.SubReml)
        assert(reml["newobject", "somekey"] is Int)
        assert(reml["newobject", "someotherkey"] is String)
        assert(reml["newobject", "somekey"] == 10)
        assert(reml["newobject", "someotherkey"] == "hello world!")
        assert(reml.getSize() == 13)
        assert((reml["newobject"] as Reml.SubReml).getSize() == 2)

        reml.obj("newobject")

        reml.obj("newobject").remove("somekey")
        assert(reml.getSize() == 13)

        println(reml)
    }
}