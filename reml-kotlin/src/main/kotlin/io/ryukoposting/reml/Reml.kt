package io.ryukoposting.reml

import java.io.File

class Reml private constructor(private val contents: MutableMap<String,Any>) {
    companion object {
        fun loadFromString(str: String): Reml {
            val out = mutableMapOf<String,Any>()
            var subObject: Pair<String,MutableMap<String,Any>>? = null

            str.split('\n').map{
                it.trim()                                       // trim leading and ending whitespace
            }.filter {
                (!it.startsWith('#')) && (it.length > 1)  // filter out comments and empty lines
            }.forEach {
                if (it.endsWith(':') && it.startsWith(':'))
                    throw RemlException("Illegal formatting: starting and ending colons in '$it'")

                else if (it.endsWith(':')) {
                    // start of new parent object
                    if (subObject != null) out[subObject!!.first] = subObject!!.second
                    subObject = Pair(it.removeSuffix(":"), mutableMapOf())

                } else if (it.startsWith(':')) {
                    // line is a member of an object; check that an object has been declared,
                    // and check for multiple declarations of the key within this object, then put
                    with(parseStringPair(it.removePrefix(":"))) {
                        (subObject ?: throw RemlException("Illegal formatting: child element with no parent in '$it'")).second
                                .also {
                                    if (it.containsKey(first))
                                        throw RemlException("Illegal formatting: multiple declarations of '${subObject!!.first}::$first'")
                                } [first] = second
                    }

                } else {
                    // line not part of a parent object
                    if (subObject != null) out[subObject!!.first] = subObject!!.second
                    subObject = null
                    with(parseStringPair(it)) {
                        if (out.containsKey(first))
                            throw RemlException("Multiple declarations of key '$first'")
                        out[first] = second
                    }
                }
            }

            // if the last thing in the reml file was an object member, this makes sure
            // the object is added to the output
            if (subObject != null) out[subObject!!.first] = subObject!!.second

            return Reml(contents = out)
        }

        fun loadFromPath(filePath: String) = loadFromString(File(filePath).readText())

        fun loadFromFile(file: File) = loadFromString(file.readText())

        private fun parseStringPair(str: String): Pair<String,Any> {
            if (!str.contains(' '))
                throw RemlException("Illegal formatting: no delimiter in line '$str'")
            with(str.replace("\\s+(?=([^\"]*\"[^\"]*\")*[^\"]*\$)".toRegex(), " ")
                    .split(" ", limit = 2)) {

                if (get(1).startsWith('\"')) {
                    if (!get(1).endsWith('\"'))
                        throw RemlException("Illegal formatting: string must be enclosed by quotes (in line '$str'")
                    return Pair(get(0), get(1).removeSurrounding("\""))
                } else if (get(1).toIntOrNull() != null)
                    return Pair(get(0), get(1).toInt())
                else if (get(1).toDoubleOrNull() != null)
                    return Pair(get(0), get(1).toDouble())
                else if ((get(1).toLowerCase() == "true") || (get(1).toLowerCase() == "false"))
                    return Pair(get(0), get(1).toBoolean())
            }
            // if we didn't return yet, then the line was invalid
            throw RemlException("Ilegal formatting in line '$str'")
        }

    }

    class SubReml (private val contents: MutableMap<String,Any>) {
        operator fun set(key: String, value: String)  { contents[key] = value }
        operator fun set(key: String, value: Int)     { contents[key] = value }
        operator fun set(key: String, value: Double)  { contents[key] = value }
        operator fun set(key: String, value: Float)   { contents[key] = value.toDouble() }
        operator fun set(key: String, value: Boolean) { contents[key] = value }

        override fun toString(): String = Reml(contents).toString()

        fun remove(key: String) = contents.remove(key)

        operator fun get(key: String) = contents[key]

        fun getSize() = contents.size
    }

    override fun toString(): String {
        return contents.map { (k, v) ->
            Pair(k as CharSequence, v)
        }.joinToString(separator = "\n") { (k, v) ->
            val valstr = when (v) {
                is String -> "\"$v\""
                is Number -> "$v"
                is Boolean -> "$v"
                is Map<*,*> -> v.map { (sk, sv) ->
                    Pair("    :$sk ".padEnd(16, ' '), sv)
                }.joinToString(separator = "\n") { (sk, sv) ->
                    when (sv) {
                        is String  -> "$sk\"$sv\""
                        is Number  -> "$sk$sv"
                        is Boolean -> "$sk$sv"
                        else -> throw RemlException("Unexpected type for value stored in object with key $k")
                    }
                } + "\n"
                else -> throw RemlException("Unexpected type for value with key $k")
            }

            val keystr = if (v is Map<*,*>) "\n$k:\n" else k.padEnd(12, ' ')

            "$keystr$valstr"
        }

    }


    operator fun set(key: String, value: String)  { contents[key] = value }
    operator fun set(key: String, value: Int)     { contents[key] = value }
    operator fun set(key: String, value: Double)  { contents[key] = value }
    operator fun set(key: String, value: Float)   { contents[key] = value.toDouble() }
    operator fun set(key: String, value: Boolean) { contents[key] = value }
    operator fun set(key: String, value: Map<String,Any>) {
        contents[key] = mutableMapOf<String,Any>().apply {
            value.forEach { k, v ->
                when (v) {
                    is String  -> put(k, v)
                    is Int     -> put(k, v)
                    is Double  -> put(k, v)
                    is Float   -> put(k, v.toDouble())
                    is Boolean -> put(k, v)
                    else -> throw RemlException("Member '$v' is not a valid REML type!")
                }
            }
        }
    }

    // NOTE: having these all referencing one set implementation caused infinite recursion
    operator fun set(objkey: String, key: String, value: String)  {
        when (contents[objkey]) {
            null -> contents[objkey] = mutableMapOf<String,Any>(Pair(key, value))
            is MutableMap<*,*> -> (contents[objkey] as MutableMap<String,Any>)[key] = value
            else -> contents[objkey] = mutableMapOf<String,Any>(Pair(key, value))
        }
    }
    operator fun set(objkey: String, key: String, value: Int)     {
        when (contents[objkey]) {
            null -> contents[objkey] = mutableMapOf<String,Any>(Pair(key, value))
            is MutableMap<*,*> -> (contents[objkey] as MutableMap<String,Any>)[key] = value
            else -> contents[objkey] = mutableMapOf<String,Any>(Pair(key, value))
        }
    }
    operator fun set(objkey: String, key: String, value: Double)  {
        when (contents[objkey]) {
            null -> contents[objkey] = mutableMapOf<String,Any>(Pair(key, value))
            is MutableMap<*,*> -> (contents[objkey] as MutableMap<String,Any>)[key] = value
            else -> contents[objkey] = mutableMapOf<String,Any>(Pair(key, value))
        }
    }
    operator fun set(objkey: String, key: String, value: Boolean) {
        when (contents[objkey]) {
            null -> contents[objkey] = mutableMapOf<String,Any>(Pair(key, value))
            is MutableMap<*,*> -> (contents[objkey] as MutableMap<String,Any>)[key] = value
            else -> contents[objkey] = mutableMapOf<String,Any>(Pair(key, value))
        }
    }


    operator fun get(key: String) = when(contents[key]) {
        is MutableMap<*, *> -> SubReml(contents = (contents[key] as MutableMap<String,Any>))
        else -> contents[key]
    }
    operator fun get(objkey: String, key: String) = when(contents[objkey]) {
        is MutableMap<*, *> -> SubReml(contents = (contents[objkey] as MutableMap<String,Any>))[key]
        else -> throw RemlException("Getter is invalid for the entry type.")
    }

    fun remove(key: String) = contents.remove(key)

    fun getSize() = contents.size

    fun obj(key: String) = SubReml(contents = (contents[key] as MutableMap<String,Any>))

}